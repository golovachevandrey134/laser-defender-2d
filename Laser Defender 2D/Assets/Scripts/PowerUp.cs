using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField] float speed = 3f;

    AudioPlayer audioPlayer;

    private void Awake()
    {
        audioPlayer = FindObjectOfType<AudioPlayer>();
    }

    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = -transform.up * speed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            audioPlayer.PlayPowerUpTakenClip();
            var health = other.GetComponent<Health>();

            if(health != null)
            {
                health.Cure();
            }

            Destroy(gameObject);
        }
    }
}
