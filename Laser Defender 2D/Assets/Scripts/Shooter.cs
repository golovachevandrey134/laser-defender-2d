using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [Header("General")]
    [SerializeField] GameObject projectilePrefab;
    [SerializeField] float projectileSpeed = 10f;
    [SerializeField] float projectileLifetime = 5f;
    [SerializeField] float baseFiringRate = 0.3f;

    [Header("AI")]
    [SerializeField] float minFiringRate = 0.1f;
    [SerializeField] float maxFiringRate = 2f;
    [SerializeField] bool useAI;

    [HideInInspector] public bool isFireing;

    Coroutine firingCoroutine;
    AudioPlayer audioPlayer;

    private void Awake()
    {
        audioPlayer = FindObjectOfType<AudioPlayer>();
    }

    void Start()
    {
        if (useAI)
        {
            isFireing = true;
        }
    }

    void Update()
    {
        Fire();
    }

    private void Fire()
    {
        if (isFireing && firingCoroutine == null)
        {
            firingCoroutine = StartCoroutine(FireContinuosly());
        }
        else if (!isFireing && firingCoroutine != null)
        {
            StopCoroutine(firingCoroutine);
            firingCoroutine = null;
        }
    }

    IEnumerator FireContinuosly()
    {
        while (true)
        {
            GameObject instance = Instantiate(projectilePrefab, transform.position, Quaternion.identity);

            Rigidbody2D rb = instance.GetComponent<Rigidbody2D>();

            if (rb != null)
            {
                rb.velocity = transform.up * projectileSpeed;
            }

            Destroy(instance, projectileLifetime);

            audioPlayer.PlayShootingClip();

            if (!useAI)
            {
                yield return new WaitForSeconds(baseFiringRate);
            }
            else if (useAI)
            {
                float firingSpeed = UnityEngine.Random.Range(minFiringRate, maxFiringRate);
                yield return new WaitForSeconds(firingSpeed);
            }
        }
    }
}
