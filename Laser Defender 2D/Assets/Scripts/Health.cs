using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] int health = 50;
    [SerializeField] ParticleSystem hitEffect;
    [SerializeField] bool applyCameraShake = true;
    [SerializeField] int scoreForDestruction = 100;
    [SerializeField] bool isPlayer;

    int maxHealth;

    CameraShake cameraShake;
    AudioPlayer audioPlayer;
    ScoreKeeper scoreKeeper;
    LevelManager levelManager;
    PowerUpsManager powerUpsManager;
    Vector2 hitPosition;   

    void Awake()
    {
        cameraShake = Camera.main.GetComponent<CameraShake>();
        audioPlayer = FindObjectOfType<AudioPlayer>();
        scoreKeeper = FindObjectOfType<ScoreKeeper>();
        levelManager = FindObjectOfType<LevelManager>();
        powerUpsManager = FindObjectOfType<PowerUpsManager>();

        maxHealth = health;
    }

    public int GetHealth()
    {
        return health;
    }

    public void Cure()
    {
        health = maxHealth;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.GetComponent<DamageDealer>();

        if (damageDealer != null)
        {
            TakeDamage(damageDealer.GetDamage());
            PlayHitEffect();
            ShakeCamera();
            damageDealer.Hit();
            audioPlayer.PlayDamgeTakenClip();
            hitPosition = other.transform.position;
        }
    }

    void ShakeCamera()
    {
        if (cameraShake != null && applyCameraShake)
        {
            cameraShake.Play();
        }
    }

    void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        if (!isPlayer && scoreKeeper != null)
        {
            scoreKeeper.ModifyScore(scoreForDestruction);
            SpawnPowerUps(hitPosition);
        }
        else
        {
            levelManager.LoadGameOver();
        }

        Destroy(gameObject);
    }

    private void SpawnPowerUps(Vector2 position)
    {
        float spawnPossibility = UnityEngine.Random.Range(0, 1f);

        if(spawnPossibility > 0.8f)
        {
            powerUpsManager.CreateMedkit(position);           
        }
    }

    void PlayHitEffect()
    {
        if (hitEffect != null)
        {
            ParticleSystem instance = Instantiate(hitEffect, transform.position, Quaternion.identity);
            Destroy(instance.gameObject, instance.main.duration + instance.main.startLifetime.constantMax);
        }
    }
}
