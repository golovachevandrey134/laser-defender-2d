using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIDisplay : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] Slider healthBar;

    ScoreKeeper scoreKeeper;
    Health playerHealth;
    int maxHealth;

    private void Awake()
    {
        scoreKeeper = FindObjectOfType<ScoreKeeper>();
        playerHealth = FindObjectOfType<Player>().GetComponent<Health>();
    }

    void Start()
    {
        scoreText.text = 0.ToString();
        healthBar.maxValue = playerHealth.GetHealth();        
    }


    void Update()
    {
        if(scoreKeeper != null)
        {
            scoreText.text = scoreKeeper.GetScore().ToString("00000000");
        }

        if(healthBar != null)
        {
            healthBar.value = playerHealth.GetHealth();
        }
    }
}
