using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpsManager : MonoBehaviour
{    
    [SerializeField] GameObject medkitPrefab;

    public void CreateMedkit(Vector2 position)
    {
        GameObject medkitInstance = Instantiate(medkitPrefab, position, Quaternion.identity);
    }
}
